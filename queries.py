from typing import Counter
from DbConnector import DbConnector
from tabulate import tabulate
from haversine import haversine, Unit
from operator import itemgetter
import numpy as np
import os
import re
import time

class Queries:
    def __init__(self):
        self.connection = DbConnector()
        self.db_connection = self.connection.db_connection
        self.cursor = self.connection.cursor
        

    def task1(self):
        users = 0
        trackpoints = 0
        activities = 0
        query1 = "SELECT COUNT(*) FROM User"
        query2 = "SELECT COUNT(*) FROM Activity"
        query3 = "SELECT COUNT(*) FROM TrackPoint"
        self.cursor.execute(query1)
        users = self.cursor.fetchone()

        self.cursor.execute(query2)
        activities = self.cursor.fetchone()

        self.cursor.execute(query3)
        trackpoints = self.cursor.fetchone()

        test_list = ([users[0], 'User'], [activities[0] , 'Activity'], [trackpoints[0], 'trackpoints'])

        print(tabulate(test_list))

    
    def task2(self):
        query = """
                SELECT COUNT(id) 
                FROM Activity 
                GROUP BY user_id 
                ORDER BY COUNT(id) ASC 
                LIMIT 1
                """
        self.cursor.execute(query)
        min_activity = self.cursor.fetchone()

        query = """
                SELECT COUNT(id) 
                FROM Activity 
                GROUP BY user_id 
                ORDER BY COUNT(id) DESC 
                LIMIT 1
                """
        self.cursor.execute(query)
        max_activity = self.cursor.fetchone()

        query = """
                SELECT AVG(count) FROM
                (SELECT COUNT(id) as count
                FROM Activity GROUP BY user_id)
                 as Counts
                """
        self.cursor.execute(query)
        avg = self.cursor.fetchone()

        test_list = ([min_activity[0], 'min activity'], [max_activity[0], 'max activity'], [avg[0], "Average"])

        print(tabulate(test_list))

    def task3(self):
        query = """
                SELECT user_id, COUNT(id) 
                FROM Activity 
                GROUP BY user_id 
                ORDER BY COUNT(id) DESC 
                LIMIT 10
                """
        self.cursor.execute(query)
        users = self.cursor.fetchall()
        print(tabulate(users, headers=['User','# of activities']))

    #find the number of activities for users that have started the activity in one day and ended the activity the next day
    def task4(self):
        query = """
                SELECT COUNT(id)
                FROM User 
                WHERE id IN (SELECT user_id 
                            FROM Activity 
                            WHERE DAYOFYEAR(ADDDATE(start_date_time, INTERVAL 1 DAY)) = DAYOFYEAR(end_date_time) 
                            GROUP BY user_id)
                """
        self.cursor.execute(query)
        activities = self.cursor.fetchall()
        print(tabulate(activities, headers=['# of users']))


    def task5(self):
        query = """
                SELECT user_id, transportation_mode, start_date_time, end_date_time 
                FROM Activity 
                GROUP BY user_id, transportation_mode, start_date_time, end_date_time 
                HAVING COUNT(*)>1
                """
        self.cursor.execute(query)
        duplicate_activities = self.cursor.fetchall()
        print(tabulate(duplicate_activities))

    def task6(self):
        users_close = []
        query1 = """
                SELECT DISTINCT user_id 
                FROM Activity
                """
        self.cursor.execute(query1)
        users = [item[0] for item in self.cursor.fetchall()]
        #Loops all users in Activity
        for user in users:
            query2 = """
                    SELECT id 
                    FROM Activity
                    WHERE user_id='%s'
                    """
            self.cursor.execute(query2 % (user))
            #Get all id's of users activity
            users_activities = [item[0] for item in self.cursor.fetchall()]

            query3 = """
                    SELECT id, user_id
                    FROM Activity
                    WHERE user_id NOT LIKE '%s'    
                    """
            self.cursor.execute(query3 % (user))
            #Get all id's of all other users activity
            other_users_activities = [list(item) for item in self.cursor.fetchall()]

            query4 = """
                        SELECT TIMESTAMPDIFF(MINUTE, date_time, CURDATE()), latitude, longitude
                        FROM TrackPoint
                        WHERE activity_id=%s
                        """

            user_datelonglat = []
            #Get all trackpoints from all of users activities
            for user_activity in users_activities:
                self.cursor.execute(query4 % (user_activity))
                user_datelonglat.append(self.cursor.fetchall())
                print (user_datelonglat)

            #Get all trackpoints from all other users
            other_user_datelonglat = []
            for other_users_activitiy in other_users_activities:
                self.cursor.execute(query4 % (other_users_activitiy[0]))
                #store user, date, long and lat
                other_user_datelonglat.append([other_users_activitiy[1], self.cursor.fetchall()])
            
            for datelonglat in user_datelonglat:
                time = datelonglat[0]
                longlat = (datelonglat[1], datelonglat[2])
                for other_datelonglat in other_user_datelonglat:
                    other_user = other_datelonglat[0]
                    time2 = other_datelonglat[1]
                    longlat2 = (other_datelonglat[2], other_datelonglat[3])
                    #if the absolute time subtraction is less or equal to 1. (selects time as minutes since current date in query 4)
                    if (abs(time-time2) <= 1):
                        #And if distance between long-lats less than 100 meters
                        if(haversine(longlat, longlat2, unit='m') <= 100):
                            #append users to list
                            users_close.append([user, other_user])

            for item in users_close:
                print('User ' + item[0] + ' and user ' + item[1] + ' were close in time and distance')

        

    def task7(self):
        query1 = """
                SELECT id 
                FROM User 
                WHERE id NOT IN (SELECT user_id 
                                FROM Activity a 
                                WHERE "taxi\n" IN (SELECT transportation_mode 
                                                    FROM Activity aa 
                                                    WHERE a.user_id=aa.user_id))"""
        self.cursor.execute(query1)
        users_no_taxi = self.cursor.fetchall()
        print(tabulate(users_no_taxi, headers=['Not taxi']))


    def task8(self):
        query = """
                SELECT transportation_mode, COUNT(DISTINCT user_id) 
                FROM Activity 
                WHERE transportation_mode NOT LIKE "none" 
                GROUP BY transportation_mode"""
        self.cursor.execute(query)
        text = self.cursor.fetchall()
        print(tabulate(text))

    def task9(self):
        query1 = """
                SELECT YEAR(start_date_time) AS Year, MONTH(start_date_time) AS Month, COUNT(*) AS TotalActivities 
                FROM Activity GROUP BY YEAR(start_date_time), MONTH(start_date_time) 
                ORDER BY COUNT(*) DESC LIMIT 1
                """ 
        self.cursor.execute(query1)
        year_month_count = self.cursor.fetchall()
        year = year_month_count[0][0]
        month = year_month_count[0][1]
        print(tabulate(year_month_count, headers=['Year','Month', '# of activities']))

        query2 = """
                SELECT user_id AS User, COUNT(id) AS ActivityCount, SUM(TIMESTAMPDIFF(SECOND, start_date_time, end_date_time))/3600 AS HOURS 
                FROM Activity
                WHERE YEAR(start_date_time)=%s AND MONTH(start_date_time)=%s 
                GROUP BY user_id 
                ORDER BY COUNT(id) DESC 
                LIMIT 2
                """
        values = (year, month)
        self.cursor.execute(query2 % (values))
        users = self.cursor.fetchall()
        print(tabulate(users, headers=['User', '# of activities', 'Hours']))



    def task10(self):
        query1 = """
                SELECT id 
                FROM Activity 
                WHERE user_id="112" AND transportation_mode="walk\n" AND YEAR(start_date_time)=2008
                """
        self.cursor.execute(query1)
        #Fetch all activity id's
        activity_ids = [activity_ids[0] for activity_ids in self.cursor.fetchall()]    
        kilometers = 0
        #Loops throug all activity ids
        for activity_id in activity_ids:
            query2 = """
                    SELECT latitude, longitude 
                    FROM TrackPoint 
                    WHERE activity_id=%s
                    """
            self.cursor.execute(query2 % (activity_id))
            points = self.cursor.fetchall()
            last_point = ()
            #Loops through all points
            for point in points:
                if last_point:
                    #Calculates total kilometers
                    kilometers += haversine(last_point, point)
                #Update last point
                last_point = point
        #Prints the result
        print('Distance walked by user 112 in 2008: ' + str(round(kilometers, 2)) + 'km')


    def task11(self):
        sum_trackpoints = []
        query1 = """
                SELECT DISTINCT id 
                FROM User
                """
        self.cursor.execute(query1)
        users = self.cursor.fetchall()
        users = np.array(users)
        #Loops through every user
        for user in users:
            sum = 0
            user = user[0]
            query = """
                    SELECT id 
                    FROM Activity 
                    WHERE user_id=%s
                    """
            self.cursor.execute(query % user)
            #Fetch all users activity
            activities = [item[0] for item in self.cursor.fetchall()] 
            #Loops throug all users activity
            for activity in activities:
                query = """
                        SELECT altitude 
                        FROM TrackPoint 
                        WHERE activity_id=%s
                        """
                self.cursor.execute(query % activity)
                altitudes = [item[0] for item in self.cursor.fetchall()]
                old_altitude=0
                #Loops throug all activitys altitudes
                for altitude in altitudes:     
                    #Check if it is not fist altitude in activity          
                    if(old_altitude!=0): 
                        #Checs if altitude is bigger than last and valis
                        if(altitude > old_altitude and old_altitude != -777):
                            #Sums up altitude difference
                            sum += (altitude - old_altitude)
                    old_altitude=altitude
            #Divide by 3.28 to get meters
            sum = sum // 3.28
            #Append to list 
            sum_trackpoints.append([user, sum])
        #Sorts by altitude
        sum_trackpoints = sorted(sum_trackpoints, key=itemgetter(1), reverse=True)
        #prints top 20
        print(tabulate(sum_trackpoints[0:20], headers=['User', 'Total gained altitude']))

    def task12(self):
        query0 = """
                SELECT DISTINCT user_id 
                FROM Activity
                """
        self.cursor.execute(query0)
        users = [user[0] for user in self.cursor.fetchall()]
        users_and_count = []
        #Loops through all users
        for user in users:
            value = str(user)
            query1 = """
                    SELECT DISTINCT Activity_id 
                    FROM TrackPoint 
                    WHERE Activity_id IN (SELECT id 
                                        FROM Activity 
                                        WHERE USER_ID = "%s")
                    """
            self.cursor.execute(query1 % (value))
            activities = [activity_id[0] for activity_id in self.cursor.fetchall()]
            count = 0
            #Loops through all activities for the user
            for activitity_id in activities:
                value = activitity_id
                query2 = """
                        SELECT TIMESTAMPDIFF(SECOND, date_time, CURDATE())/60 
                        FROM TrackPoint 
                        WHERE Activity_id = %s
                        """
                self.cursor.execute(query2 % (value))
                date_times = [date_time[0] for date_time in self.cursor.fetchall()]
                old_date_time = 0
                #Loops through all date times for the activity
                for date_time in date_times:
                    #Check if it is not first date time in activity
                    if(old_date_time!=0):
                        #check if last date time is bigger than date time plus 5 minutes
                        if(old_date_time >= date_time+5):
                            count += 1
                            #Break if activity is invalid
                            break
                    #Update last date time
                    old_date_time = date_time
            #Append user and users result to list
            users_and_count.append([user, count])
            if (len(users_and_count) == 20):                     # REMOVE IF YOU WANT ALL 
                break                                            # 
        print(tabulate(users_and_count, headers=['user', '# of invalid entries']))


                    

                    


                


        
        





