from typing import Counter
from DbConnector import DbConnector
from tabulate import tabulate
from queries import *
import os
import re
import time

class Program:

    # Initialize database
    def __init__(self):
        self.connection = DbConnector()
        self.db_connection = self.connection.db_connection
        self.cursor = self.connection.cursor

    # Create user table
    def create_user_table(self):
        query = """CREATE TABLE IF NOT EXISTS User (
                   id VARCHAR(4) NOT NULL PRIMARY KEY,
                   has_labels BOOL)
                """
        self.cursor.execute(query)
        self.db_connection.commit()

    # Creates file name from one trackpoint line to original file format
    def create_filename(self, datetime):
        if(len(datetime) < 20):
            print('error i create filename')
        filename = datetime[-20:]
        filename = filename[0:4] + filename[5:7] + filename[8:10] + filename[11:13] + filename[14:16] + filename[17:19] + '.plt'
        return filename

        
    # Creates Activity table
    def create_activity_table(self):
        query = """CREATE TABLE IF NOT EXISTS Activity (
                id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                user_id VARCHAR(4),
                transportation_mode VARCHAR(50) DEFAULT 'NULL',
                start_date_time DATETIME,
                end_date_time DATETIME,
                FOREIGN KEY (user_id) REFERENCES User(id)
                )
                """
        self.cursor.execute(query)
        self.db_connection.commit()

    # Creates TrackPoint table
    def create_trackpoint_table(self):
        query = """CREATE TABLE IF NOT EXISTS TrackPoint (
                id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
                activity_id INT,
                latitude DOUBLE(8,6),
                longitude DOUBLE(9,6),
                altitude INT,
                date_days DOUBLE(28,15),
                date_time DATETIME,
                FOREIGN KEY (activity_id) REFERENCES Activity(id)
                )
                """
        self.cursor.execute(query)
        self.db_connection.commit()

    # Returns all users
    def get_users(self):
        users = []
        users = os.listdir("../testset/Data")
        return users

    # Returns all users that have transportation modes
    def get_user_with_labels(self):
        with open('../dataset/labeled_ids.txt') as f:
            allUsers = f.read().splitlines()
        f.close()
        return allUsers

    # Inserts users into User table
    def insert_users(self):
        
        # Retrives all users with/without transportation modes
        all_users = Program.get_users(self)
        all_labeld_users = Program.get_user_with_labels(self)

        #loops through all users and inserts into database
        for user in all_users:
            has_label = False
            if user in all_labeld_users:
                has_label = True
            query = "INSERT INTO User (id, has_labels) VALUES ('%s', %s)"
            self.cursor.execute(query % (user, has_label))
        self.db_connection.commit()

    # Returns date and time from a trackpoint line in correct format
    def get_datetime(self, line):
        if(len(line) < 20):
            print('error i create filename')
        datetime = line[-20:]
        datetime = datetime.replace(',', ' ')
        print(datetime)
        return datetime

    # inserts all activities in Actvity table
    def insert_activity(self):
        # Retrieves all users and loops through all
        all_users = Program.get_users(self)
        for user in all_users:
            path = '../testset/Data/' + user + '/Trajectory/'
            all_files = os.listdir(path)
            # Loops through all activities for one user
            for file in all_files:
                #declare variables for query insert
                start_time = ''
                end_time = ''
                transportation = 'none'
                query = ''
                # Opens one file at a time and retrieves all lines
                with open(path + file) as f:
                    lines = f.readlines()
                    # Checks if file has a transportation mode in first line or something else in the first six lines, then adds it to the query 
                    if ('Geolife' in lines[0]):
                        start_time = Program.get_datetime(self, lines[6]) 
                        end_time = Program.get_datetime(self, lines[-1])
                        values = (user, transportation, start_time, end_time)
                        query = "INSERT INTO Activity (user_id, transportation_mode, start_date_time, end_date_time) VALUES ('%s', '%s', '%s', '%s')"
                    
                    else:
                        start_time = Program.get_datetime(self, lines[1])
                        end_time = Program.get_datetime(self, lines[-1])
                        transportation = lines[0]
                        values = (user, transportation, start_time, end_time)
                        query = "INSERT INTO Activity (user_id, transportation_mode, start_date_time, end_date_time) VALUES ('%s', '%s', '%s', '%s')"
                
                # Executes query and commits
                self.cursor.execute(query % (values))
        self.db_connection.commit()

    # Retrieves correct activity id for one user at a given time
    def get_activity_id(self, user, time):
        query = "SELECT Activity.id FROM Activity WHERE Activity.start_date_time = '%s' AND Activity.user_id = '%s'"
        values = (time, user)
        self.cursor.execute(query % (values))
        result = self.cursor.fetchone()
        return(result[0])

    # Inserts trackpoints
    def insert_trackpoints(self):

        #Retrieves all users and loops through them
        all_users = Program.get_users(self)
        for user in all_users:
            path = '../testset/Data/' + user + '/Trajectory/'
            all_files = os.listdir(path)
            # Loops through all files
            for file in all_files: 
                # Opens one file at a time
                with open(path + file) as f:
                    lines = f.readlines()
                    #Checks if file should start reading from the 7th or 1st line, depending on if file contains trnasportation mode
                    if ('Geolife' in lines[0]):
                        lines = lines[6:]
                    else:
                        lines = lines[1:]
                    start_time = lines[0].split(',')
                    start_time = start_time[5] + ' ' + start_time[6]
                    id_a = Program.get_activity_id(self, user, start_time)
                    # loops through all lines and inserts them into the query for all files and all users
                    for line in lines:
                        elements = line.split(',')
                        lat = elements[0]
                        long = elements[1]
                        alt = elements[3]
                        days = elements[4]
                        date = elements[5] + ' ' + elements[6]
                        values = (id_a, lat, long, alt, days, date)
                        query = "INSERT INTO TrackPoint (activity_id, latitude, longitude, altitude, date_days, date_time) VALUES (%s, %s, %s, %s, %s, '%s')"
                        self.cursor.execute(query % (values))
                    self.db_connection.commit()
                            
                
    # Removes all empty files (files that have no more than 6 lines) from labeled users
    def remove_empty_files(self):
        labeld_users = Program.get_user_with_labels(self)
        for user in labeld_users:
            path = '../testset/Data/' + user + '/Trajectory/'
            all_files = os.listdir(path)
            for file in all_files:
                with open(path + file) as f:
                    lines = f.readlines()

                    if(len(lines) < 7 and 'Geolife' in lines[0]):
                        f.close()
                        os.remove(path + file)
                        print(lines)  

    # Distributes data for labeled users so that every activity comes in eperate files
    def clean_data(self):
        # Retrieves labeled users and loops through all
        labeld_users = Program.get_user_with_labels(self)
        for user in labeld_users:  

            # Retrieves path to labels.txt and all activities                                                    
            path = '../testset/Data/' + user + '/Trajectory/'
            allFiles = os.listdir(path)                                         
            label_path = '../testset/Data/' + user + '/labels.txt'   

            # Retrieves all lines from labels.txt   
            with open(label_path) as f:                                 
                lines = f.readlines()[1:]
                f.close()
            # Loops through all lines in labels_txt file    
            for line in lines:           

                # Retrieves transportation mode                           
                transportation_mode = line.split('\t')[2]                                  

                # Retrieves start time
                start_time_transportation = line[0:19]              
                start_time_transportation = start_time_transportation.replace('/', '-') 
                start_time_transportation = start_time_transportation.replace(' ', ',') 

                # Retrieves end time   
                end_time_transportation = line[20:39]               
                end_time_transportation = end_time_transportation.replace('/', '-')
                end_time_transportation = end_time_transportation.replace(' ', ',')

                # Loops through all activity files
                for files in allFiles:                            
                    tp_lines = []               # All trackpoint lines in one file
                    oldfile_data = []           # Data to be set in old file
                    start_index = -1            # Start and end index for the trackpoint line where the
                    end_index = -1              # activity with transportation mode starts and stops

                    # Opens the correct file where the labels.txt line date matches the name of the file
                    if files.startswith(start_time_transportation[0:4] + start_time_transportation[5:7] + start_time_transportation[8:10]): 
                        with open('../testset/Data/' + user + '/Trajectory/' + files) as ff:

                            # Retrieves all lines from current file
                            tp_lines = ff.readlines()

                            # Retrieves start index of the line where the activity starts
                            for tp_line in tp_lines:
                                if (start_time_transportation in tp_line):
                                    start_index = tp_lines.index(tp_line)
                                    break

                            # Retreives end index of the line where the activity ends            
                            for tp_line in tp_lines:
                                if (end_time_transportation in tp_line):
                                    end_index = tp_lines.index(tp_line)
                                    break

                            # Both start and end index needs to exist in the file    
                            if(start_index != -1 and end_index != -1):
                                newfile_data = tp_lines[start_index:end_index+1]      #data to be written to new file 

                                # Appends all trackpoints that is not included in any transportation mode to a separate file                     
                                for start in tp_lines[:start_index]:
                                    oldfile_data.append(start)
                                for end in tp_lines[end_index:]:
                                    oldfile_data.append(end)

                                # Creates two new files
                                filename_new = Program.create_filename(self, newfile_data[0])
                                if(len(oldfile_data) > 6):                                          # checks if there still is data when activity trackpoints are extracted
                                    filename_old = Program.create_filename(self, oldfile_data[6])
                                
                                # Sets transportation mode to first index in the array
                                newfile_data.insert(0, transportation_mode)

                                # Closes and removes old file
                                ff.close()
                                os.remove(path + files)

                                # Opens new file and writes all trackpoints with transportation mode
                                newf = open(path + filename_new, 'x')
                                for file_line in newfile_data:
                                    newf.write(file_line)
                                newf.close()
                                
                                # Writes rest of data that does not have activity with transportation mode
                                if(len(oldfile_data) > 6):
                                    oldf = open(path + filename_old, 'x')
                                    for file_line in oldfile_data:
                                        oldf.write(file_line)
                                    oldf.close()

    # Deletes all files with more than 2500 trackpoints
    def delete_big_files(self):

        # Retrieves all users and loops through them
        users = Program.get_users(self)
        for user in users:
            path = '../dataset/Data/' + user + '/Trajectory/'
            allFiles = os.listdir(path)

            # Loops through all files and checks if file has too many lines
            for file in allFiles:
                if (sum(1 for line in open(path+file)) > 2506):
                    os.remove(path+file)


# main
def main():
    program = None
    try:
        query = Queries()
        # To execute task:
        # query.task<taskNumber>()
    except Exception as e:
        print("ERROR: Failed to use database:", e)
    finally:
        if program:
            program.connection.close_connection()


if __name__ == '__main__':
    main()
